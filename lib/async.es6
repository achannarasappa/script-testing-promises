const Q = require('q');
const _ = require('lodash');

const delayedMultiply = _.curry((a, b) => {

  const deferred = Q.defer();

  setTimeout(() => {

    if (!_.isNumber(a))
      deferred.reject('a is not a number!');

    if (!_.isNumber(b))
      deferred.reject('b is not a number!');

    if (_.isNumber(a) && _.isNumber(b))
      deferred.resolve(a * b)

  }, 5000);

  return deferred.promise;

});

const add = (a, b) => Q.fcall(() => {

  if (!_.isNumber(a))
    throw new Error('a is not a number!');

  if (!_.isNumber(b))
    throw new Error('b is not a number!');

  return a + b;

});

const delayedAddMultiply = ({ add, delayedMultiply }, a, b, c) => {

  return add(a, b)
    .then(delayedMultiply(c))

};

export { delayedMultiply, add, delayedAddMultiply };