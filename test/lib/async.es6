const { add, delayedAddMultiply } = require('../../lib/async');

describe('async', () => {

  describe('delayedAddMultiple', () => {

    describe('Sinon and Q approach', () => {

      const Q = require('q');
      const chai = require('chai');
      const chaiAsPromised = require('chai-as-promised');
      const sinon = require('sinon');
      const sinonChai = require("sinon-chai");
      chai.use(chaiAsPromised);
      chai.use(sinonChai);
      const expect = chai.expect;

      it('should add the first two arguments then multiple the result by the third argument', () => {

        const delayedMultiplyCall2 = sinon.stub().returns(Q.resolve(12));
        const delayedMultiply = sinon.stub().returns(delayedMultiplyCall2);
        const dependencies = { delayedMultiply, add };
        const promise = delayedAddMultiply(dependencies, 2, 2, 3);

        return Q.all([
          expect(promise).to.eventually.eql(12),
          expect(promise).to.be.fulfilled.then(() => {
            expect(delayedMultiply).to.have.been.calledWith(3);
            expect(delayedMultiplyCall2).to.have.been.calledWith(4);
          })
        ])
      });

      it('should throw an error if any argument is not a number', () => {

        const delayedMultiply = sinon.stub().returns(() => Q.reject('b is not a number!'));
        const dependencies = { delayedMultiply, add };
        const promise = delayedAddMultiply(dependencies, 1, 2, '999');

        return expect(promise).to.be.rejectedWith('b is not a number!');

      });

    });

    describe('Q approach', () => {

      const Q = require('q');
      const chai = require('chai');
      const chaiAsPromised = require('chai-as-promised');
      const expect = chai.expect;
      chai.use(chaiAsPromised);

      it('should add the first two arguments then multiple the result by the third argument', () => {

        const delayedMultiply = () => () => Q.resolve(9);
        const dependencies = { delayedMultiply, add };
        const promise = delayedAddMultiply(dependencies, 1, 2, 3);

        return expect(promise).to.eventually.eql(9);

      });

      it('should throw an error if any argument is not a number', () => {

        const delayedMultiply = () => () => Q.reject('b is not a number!');
        const dependencies = { delayedMultiply, add };
        const promise = delayedAddMultiply(dependencies, 1, 2, '999');

        return expect(promise).to.be.rejectedWith('b is not a number!');

      });

    });

  });

});